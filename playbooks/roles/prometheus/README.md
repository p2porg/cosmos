# Ansible Role: prometheus

## Validate config (promtool)

### prometheus.yml template

```
validate: "{{ _prometheus_binary_install_dir }}/promtool check config %s"
```