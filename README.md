# Cosmos validator, prometheus exporter, hardening

## TODO

- Ansible: Add tags
- Ansible: Separate huge *.yml files
- Ansible: Add more validations
- Ansible: Molecule tests
- Ansible: Fix versions for requirements
- Ansible: Add YAMLLint and ansible-lint
- Lynis: Hardening system services `systemd-analyze security SERVICE`
- Hardening: Init aide, set config and updates for db
- Hardening: Periodicaly update db, run rkhunter and send reports
- Hardening: Search for identical keys in /etc/sysctl.d/
- Prometheus: Add TLS
- Prometheus: Add auth
- Prometheus: Add scrape config
- Validator: Use Key Management Systems HSM (YubiHSM 2 + Tendermint KMS) `cargo build --features yubihsm` https://github.com/iqlusioninc/tmkms/blob/main/README.yubihsm.md
- Validator: Use Sentry node (DDOS Protection)
- Validator: Add settings for mainnet
- Validator: Set Cosmovisor for updates
- Validator: Add sync options (fast or state)

### Ansible: Find with loop for pattern

It works
```yaml
- name: Find all compilers
  find:
    paths:
      - /usr/bin
      - /bin
    patterns:
      - gcc
      - g++
      - cc
      - c++
      - clang
      - clang++
      - make
  register: compiler_files

- name: Set permissions for compilers
  file:
    path: "{{ item.path }}"
    owner: root
    group: root
    mode: '0500'
  loop:
    - "{{ compiler_files.files }}"
  when: compiler_files.matched > 0
```

It doesn't work

```yaml
compilers_list:
  - gcc
  - g++
  - cc
  - c++
  - clang
  - clang++
  - make
  - as

- name: Find all compilers
  find:
    paths:
      - /usr/bin
      - /bin
    patterns: "{{ compilers_list }}"
  register: compiler_files

- name: Set permissions for compilers
  file:
    path: "{{ item.path }}"
    owner: root
    group: root
    mode: '0500'
  loop:
    - "{{ compiler_files.files }}"
  when: compiler_files.matched > 0
```

```
FAILED! => {"msg": "'dict object' has no attribute 'files'"}
```

### Lynis: bug

```
[+] Hardening
------------------------------------
    - Installed compiler(s)                                   [ FOUND ]
```

```bash
grep compiler /var/log/lynis.log
2024-06-21 19:02:30   Found known binary: as (compiler) - /usr/bin/as
2024-06-21 19:02:59 Performing test ID HRDN-7220 (Check if one or more compilers are installed)
2024-06-21 19:02:59 Test: Check if one or more compilers can be found on the system
2024-06-21 19:02:59 Result: found installed compiler. See top of logfile which compilers have been found or use /usr/bin/grep to filter on 'compiler'
2024-06-21 19:02:59 Performing test ID HRDN-7222 (Check compiler permissions)
2024-06-21 19:02:59 Test: Check if one or more compilers can be found on the system
```

```bash
ls -l  /usr/bin/as
lrwxrwxrwx 1 root root 28 Jun 21 17:35 /usr/bin/as -> /usr/bin/x86_64-linux-gnu-as
```

```bash
ls -l /usr/bin/x86_64-linux-gnu-as
-r-x------ 1 root root 696624 Jan 23 10:47 /usr/bin/x86_64-linux-gnu-as
```

Issue created
https://github.com/CISOfy/lynis/issues/1514

## Ansible playbook

### Local run

```
git clone git@gitlab.com:p2porg/cosmos.git
cd cosmos
```

#### From host

```
ansible-playbook --inventory inventories --user <USER> --limit local --extra-vars "ansible_become_pass=<PASSWORD>" playbooks/main.yml
```

#### From docker 

##### Windows
```
docker run -it --rm --volume %cd%:/app --workdir /app alpinelinux/ansible sh -c "chmod -v 600 id_rsa_test && ansible-galaxy collection install --requirements-file requirements.yml && ansible-playbook --inventory inventories --user <USER> --limit local --key-file id_rsa_test --ask-vault-pass playbooks/main.yml"
```
or, for quick test, create `vault-password-file`:
```
docker run -it --rm --volume %cd%:/app --workdir /app alpinelinux/ansible sh -c "chmod -v 600 id_rsa_test vault-password-file && ansible-galaxy collection install --requirements-file requirements.yml && ansible-playbook -i inventories --user <USER> --key-file id_rsa_test --limit local --vault-password-file vault-password-file --ssh-common-args '-o StrictHostKeyChecking=no' playbooks/main.yml"
```

##### Linux
```
docker run -it --rm --volume %pwd%:/app --workdir /app alpinelinux/ansible sh -c "chmod -v 600 id_rsa_test && ansible-galaxy collection install --requirements-file requirements.yml && ansible-playbook --inventory inventories --user <USER> --limit local --key-file id_rsa_test --ask-vault-pass playbooks/main.yml"
```

## Cosmos testnet validator

### Installation & Configuration (workstation)
```
wget --output-file=gaiad https://github.com/cosmos/gaia/releases/download/v17.2.0/gaiad-v17.2.0-linux-amd64
chmod +x gaiad
NODE_MONIKER=local # local, dev, test
mkdir -p ~/${NODE_MONIKER}
```

#### Tendermint/CometBFT key (cosmosvalconspub): A unique key that is used to sign consensus votes. (workstation)
```
gaiad init ${NODE_MONIKER} --chain-id theta-testnet-001 --home ~/${NODE_MONIKER}
gaiad tendermint show-validator --home ~/${NODE_MONIKER}
```
Additional settings for testnet:
- `config/app.toml`: `minimum-gas-prices = 0.005uatom`
- `config/config.toml`: `seeds = 639d50339d7045436c756a042906b9a69970913f@seed-01.theta-testnet.polypore.xyz:26656,3e506472683ceb7ed75c1578d092c79785c27857@seed-02.theta-testnet.polypore.xyz:26656`
- `config/config.toml`: `enable = true`
- `config/config.toml`: `rpc_servers = https://rpc.state-sync-01.theta-testnet.polypore.xyz:443,https://rpc.state-sync-02.theta-testnet.polypore.xyz:443`

Runtime settings received from RPC, for ex.:
- `config/config.toml`: `trust_height = `
- `config/config.toml`: `trust_hash = `

```bash
SYNC_RPC_1=https://rpc.state-sync-01.theta-testnet.polypore.xyz:443
CURRENT_BLOCK=$(curl -s $SYNC_RPC_1/block | jq -r '.result.block.header.height')
TRUST_HEIGHT=$[$CURRENT_BLOCK-1000]
TRUST_HASH=$(echo $TRUST_BLOCK | jq -r '.result.block_id.hash')
sed -i -e "/trust_height =/ s/= .*/= $TRUST_HEIGHT/" $NODE_HOME/config/config.toml
sed -i -e "/trust_hash =/ s/= .*/= \"$TRUST_HASH\"/" $NODE_HOME/config/config.toml
```
For backup save files:
- `config/node_key.json` - Private key to use for node authentication in the p2p protocol.
- `config/priv_validator_key.json` - Private key to use as a validator in the consensus protocol.


#### Application key(cosmosvaloper, cosmosvaloperpub): This key is created from the gaiad binary and is used to sign transactions. (workstation)
```
gaiad keys add ${NODE_MONIKER} --home=~/${NODE_MONIKER}
gaiad keys show ${NODE_MONIKER} --bech=val --home=~/${NODE_MONIKER}
# backup key to file
gaiad keys export ${NODE_MONIKER} --home=~/${NODE_MONIKER} --output-file=${NODE_MONIKER}.keys.backup
```

#### Claim some free tokens in testnet - steaks (workstation)

Get MYPUBKEY
```bash
gaiad keys list --home=~/${NODE_MONIKER}
```

Request tokens through the faucet Discord channel https://discord.com/channels/669268347736686612/953697793476821092

For ex., for Local:
```
$request theta-testnet-001 cosmos1gxe9ewxmtvt8epdft2ek4ga9zrsqcxx29nqnf8
```

Check the balance
```bash
gaiad query bank balances cosmos1gxe9ewxmtvt8epdft2ek4ga9zrsqcxx29nqnf8 --chain-id theta-testnet-001 --home ~/${NODE_MONIKER} --node https://rpc.state-sync-01.theta-testnet.polypore.xyz:443
```

#### Run service and check status (validator)

```bash
systemctl enable --now gaiad

systemctl status gaiad
journalctl -fu gaiad
```

#### Syncronization

Run
```bash
curl http://localhost:26657/status | grep "catching_up"
```
or
```bash
su gaia
gaiad status
```
Checking the value `catching_up`:
- `"catching_up":true`: synchronization has not finished yet
- `"catching_up":false`: time to open your account in Cosmos


#### Create Validator (workstation)

```bash
NODE_MONIKER=dev # dev, test
gaiad tx staking create-validator \
  --amount=1000000uatom \
  --pubkey=$(gaiad tendermint show-validator --home ~/${NODE_MONIKER}) \
  --moniker="${NODE_MONIKER}" \
  --chain-id=theta-testnet-001 \
  --commission-rate="0.10" \
  --commission-max-rate="0.20" \
  --commission-max-change-rate="0.01" \
  --gas="auto" \
  --gas-adjustment=1.5 \
  --gas-prices="0.005uatom" \
  --from=${NODE_MONIKER} \
  --node https://rpc.state-sync-01.theta-testnet.polypore.xyz:443 \
  --home ~/${NODE_MONIKER}
```

#### Confirm Your Validator is Running (workstation)

```bash
gaiad query tendermint-validator-set --node https://rpc.state-sync-01.theta-testnet.polypore.xyz:443 | grep "$(gaiad tendermint show-address --home ~/${NODE_MONIKER})"
```

#### Check peers
```bash
curl http://localhost:26657/net_info
```

### Validator keys
- `dev`: `cosmosvaloper1k4pyeulsm8u2dgveygx7gv2ver53d26l46zlhj`
- `test`: `cosmosvaloper1gh04gs0kzcx9004nmxwtuhjuw57s2auaetajvd`

### Validator addresses
- `dev`: `cosmosvalcons1l58pw80n0vt5v6zrcxgdsm4z0qzl9lxxmp88xs`
- `test`: `cosmosvalcons1lshtxnvfvqnk2gl9a9z9wvd2300y2ljh80vlxr`


### Host secret example

Create secrets.yml in inventory
```yaml
ansible_become_pass: becomePassword
node_key: |
  {"priv_key":{"type":"tendermint/PrivKeyEd25519","value":"...
priv_validator_key: |
  {
    "address": "...
```
and then
```bash
ansible-vault encrypt secrets.yml
```

### Useful links

#### Documantation
https://hub.cosmos.network/main

#### RPC nodes
https://testnet.cosmos.directory/cosmoshubtestnet/nodes

### Explorer
https://explorer.nodestake.org/cosmos-testnet/tx/?tab=search

## Prometheus

### Local

http://172.22.77.153:9090

### Dev

http://45.92.176.76:9090

### Test

http://158.180.50.167:9090

